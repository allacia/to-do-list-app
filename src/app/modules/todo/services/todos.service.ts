import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { CacheService } from '../../../core/services/cache.service';
import { TODO_LIST } from '../components/todo-list/cache-keys';

@Injectable({
  providedIn: 'root'
})
export class TodosService {
  listOfToDo: any;
  todoList = new BehaviorSubject({});

  constructor(private cacheService: CacheService) {
    this.listOfToDo = [{
      id: 0,
      title: 'Разделение на компоненты и/или модули',
      complete: true
    },
      {
        id: 1,
        title: 'Сохранение состояния в localStorage',
        complete: true
      },
      {
        id: 2,
        title: 'Использование rxjs',
        complete: true
      },
      {
        id: 3,
        title: 'Material design',
        complete: false
      },
      {
        id: 4,
        title: 'Drag\'n\'Drop',
        complete: true
      }];
  }

  getInitTodos() {
    if (!this.cacheService.getLocalStorageByKey(TODO_LIST)) {
      this.cacheService.setLocalStorage(TODO_LIST, this.listOfToDo);
    }

    const value = this.cacheService.getLocalStorageByKey(TODO_LIST);
    this.todoList.next(value);
  }

  set(item) {
    this.cacheService.setLocalStorage(TODO_LIST, item);
    this.todoList.next(item);
  }

}
