import { Component, OnInit } from '@angular/core';
import { TodosService } from '../../services/todos.service';
import {CdkDrag, CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.scss']
})
export class TodoListComponent implements OnInit {
  listOfToDo: any;
  fullListOfToDo: any;
  formShow = false;
  newToDo = '';
  completed = [];

  constructor(private todosService: TodosService) {  }

  ngOnInit() {
    this.fullListOfToDo = this.todosService.getInitTodos();

    this.todosService.todoList.subscribe(
      todos => {
        this.fullListOfToDo = todos;
        this.listOfToDo = this.fullListOfToDo.filter(item => item.complete === false);
        this.completed = this.fullListOfToDo.filter(item => item.complete === true);
      }
    );
  }

  toggleForm() {
    this.formShow = !this.formShow;
  }

  addToDo() {
    if (!this.newToDo) {
      return;
    }

    this.toggleForm();

    let lastId = this.fullListOfToDo[this.fullListOfToDo.length - 1].id;
    const newTask = {
      id: ++lastId,
      title: this.newToDo,
      complete: false
    };
    this.fullListOfToDo.push(newTask);
    this.todosService.set(this.fullListOfToDo);
    this.newToDo = '';
  }

  removeToDo(id) {
    const deleteToDoIndex = this.fullListOfToDo.findIndex(item => item.id === id);
    this.fullListOfToDo.splice(deleteToDoIndex, 1);
    this.todosService.set(this.fullListOfToDo);
  }

  checkedToDo(id) {
    const checkToDo = this.fullListOfToDo.findIndex(item => item.id === id);
    this.fullListOfToDo[checkToDo].complete = !this.fullListOfToDo[checkToDo].complete;
    this.todosService.set(this.fullListOfToDo);
  }

  onDrop(event: CdkDragDrop<[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data,
        event.previousIndex,
        event.currentIndex);
    } else {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );

      const containerData = event.container.data;
      this.toggle(containerData[event.currentIndex]);
      this.fullListOfToDo = event.container.data.concat(event.previousContainer.data);
      this.todosService.set(this.fullListOfToDo);
    }
  }

  toggle(todo) {
    todo.complete = !todo.complete;
  }
}
