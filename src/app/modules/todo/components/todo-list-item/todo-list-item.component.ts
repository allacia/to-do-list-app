import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-todo-list-item',
  templateUrl: './todo-list-item.component.html',
  styleUrls: ['./todo-list-item.component.scss']
})
export class TodoListItemComponent implements OnInit {
  @Input() todo: any;
  @Output() id = new EventEmitter();
  @Output() checkId = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  checkToDo(checkId) {
    this.checkId.emit(checkId);
  }

  deleteToDo(id) {
    this.id.emit(id);
  }

}
