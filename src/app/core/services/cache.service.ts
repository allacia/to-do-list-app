import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CacheService {

  constructor() { }

  setLocalStorage(key: string, value: any) {
    window.localStorage.setItem(key, JSON.stringify(value));
  }

  getLocalStorageByKey(key: string) {
    return JSON.parse(window.localStorage.getItem(key));
  }

}
